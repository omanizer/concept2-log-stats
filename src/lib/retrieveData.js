const puppeteer = require('puppeteer')
const axios = require('axios')
const csvtojson = require('csvtojson')
const moment = require('moment-timezone')
const range = require('lodash/range')
const flatten = require('lodash/flatten')
const fs = require('fs')
const path = require('path')

const dataPath = path.join(__dirname, '../../data')

const run = async () => {
  await new Promise((resolve, reject) => {
    fs.mkdir(path.join(__dirname, '../../data'), { recursive: true }, (err) => {
      if (err) {
        return reject(err)
      }
      resolve()
    })
  })

  const username = process.env.C2_USERNAME
  const password = process.env.C2_PASSWORD

  if (!username || !password) {
    throw new Error('C2_USERNAME and C2_PASSWORD env vars are required')
  }

  const browser = await puppeteer.launch({
    headless: true,
    executablePath: process.env.CHROMIUM_PATH,
    args: ['--no-sandbox']
  })

  try {
    const page = await browser.newPage()
    await page.goto('https://log.concept2.com/login')

    // Fill out username/password
    const username = process.env.C2_USERNAME
    const password = process.env.C2_PASSWORD

    await page.type('#username', username)
    await page.type('#password', password)
    await page.click('form input[type=submit]')

    const cookies = await page.cookies()
    let cookieHeader = cookies.map(cookie => `${cookie.name}=${cookie.value}`).join('; ')

    await browser.close()

    let files = await new Promise((resolve, reject) => {
      fs.readdir(dataPath, (err, files) => {
        if (err) {
          return reject(err)
        }
        resolve(files)
      })
    })

    let currentYear = moment().year()
    // If it's may then the year is actually the next year
    if (moment().month() >= 4) {
      currentYear++
    }
    let yearsToGet = range(currentYear - 5, currentYear + 1)
    // Only pull current year if we have this year and previous year files
    if (files.includes(`${currentYear}.csv`) && files.includes(`${currentYear - 1}.csv`)) {
      yearsToGet = [currentYear]
    }

    console.log(`Pull years ${yearsToGet.join(', ')}`)

    let pulledFiles = await Promise.all(
      yearsToGet.map(async (year) => {
        let response = await axios({
          method: 'GET',
          url: `https://log.concept2.com/season/${year}/export`,
          headers: {
            cookie: cookieHeader
          }
        })

        // If the CSV has at least one line in it write it to the year's file
        if (response.data && response.data.split('\n').filter(l => !!(l.trim())).length > 1) {
          await new Promise((resolve, reject) => {
            fs.writeFile(path.join(__dirname, `../../data/${year}.csv`), response.data, (err) => {
              if (err) {
                return reject(err)
              }
              resolve()
            })
          })

          return response.data
        }
      })
    )

    return pulledFiles.filter(f => !!f)
  } catch (e) {
    if (browser) {
      await browser.close()
    }
    throw e
  }
}

module.exports = run
