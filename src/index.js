const Koa = require('koa')
const Router = require('koa-router')
const koaNunjucks = require('koa-nunjucks-2')
const app = new Koa()
const router = new Router()
const port = process.env.HTTP_PORT || 8080
const path = require('path')

app.use(koaNunjucks({
  ext: 'html',
  path: path.join(__dirname, 'views'),
  nunjucksConfig: {
    trimBlocks: true,
    nocache: true
  }
}))

const basicStats = require('./calc/basicStats')
const retrieveData = require('./lib/retrieveData')

router.get('/view', async (ctx) => {
  let stats = await basicStats()
  await ctx.render('index', { stats })
})

router.get('/', async (ctx) => {
  let stats = await basicStats()
  ctx.body = JSON.stringify(stats, null, 2)
})

router.get('/refresh', async (ctx) => {
  try {
    let result = await retrieveData()
    let stats = await basicStats()
    ctx.body = JSON.stringify({
      ...stats,
      status: `Pulled ${result.length} files`
    }, null, 2)
  } catch (e) {
    ctx.body = e.message
  }
})

app
  .use(router.routes())
  .use(router.allowedMethods())

app.listen(port)
console.log('app running on port', port)