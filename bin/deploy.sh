#!/bin/sh

docker rmi -f $(docker images | grep "concept2-log-stats" | awk '/ / { print $3 }')
docker build -t concept2-log-stats -f ./Dockerfile .
docker tag concept2-log-stats:latest registry.gitlab.com/omanizer/concept2-log-stats:latest
docker push registry.gitlab.com/omanizer/concept2-log-stats:latest